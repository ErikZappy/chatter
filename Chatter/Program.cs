﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Chatter
{
    public class Program
    {
        static void Main(string[] args)
        {
            ChatterClient.AysncClient.StartClient();
        }

        static void sendMsg()
        {
            /* 
            sockfd is a file descriptor used by by the i/o stream table thing
            i think
            THIS WORKS BOISE
            */

            int port;
            byte[] ipAddr;

            Console.WriteLine("Enter the hostname and port number");
            string input = Console.ReadLine();
            string[] inputs = input.Split(' ');

            if (inputs.Length != 2)
            {
                Console.WriteLine("Yo you have too many or too few entries get out");
                Environment.Exit(1);
            }

            if (!(inputs[0].All(char.IsDigit) || inputs[1].All(char.IsDigit)))
            {
                Console.WriteLine("The hostname and port number can only be numbers\n" +
                    "as of right now");
                Environment.Exit(2);
            }

            port = Convert.ToInt32(inputs[1]);
            ipAddr = ConvertStringToIPAddr(inputs[0]);

            Console.WriteLine(String.Format("IP addr: {0}\nPort #: {1}", IPAddrToString(ipAddr), port));

            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //s.Connect(new IPAddress(new byte[] { 192, 168, 1, 2 }), 11011);


            try {
                s.Connect(new IPAddress(ipAddr), port);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Client most likley didn't connect to server");
                Console.WriteLine("Check status of server and try again");
                Console.WriteLine(Dns.GetHostEntry("localhost").AddressList[0].ToString());
                Environment.Exit(4);
            }

            Console.WriteLine("Enter your message!");

            string msg = Console.ReadLine();
            byte[] byte_msg = ConvertStringToByteArray(msg);
            s.Send(byte_msg);
            Console.WriteLine("Success!");

            Console.ReadLine();
        }


        // I can just use Encoding.ASCII.GetBytes and vice verse
        // that makes things easier I guess
        static byte[] ConvertStringToByteArray(string str)
        {
            Queue<Byte> msg = new Queue<byte>();
            char[] charMsg = str.ToCharArray();
            foreach(char c in charMsg)
            {
                msg.Enqueue(Convert.ToByte(c));
            }

            return msg.ToArray();
        }

        static byte[] ConvertStringToIPAddr(string str)
        {
            Queue<Byte> ipAddr = new Queue<byte>();
            String[] bytes = str.Split('.');
            if(bytes.Length != 4)
            {
                Console.WriteLine("IP address input is not properly formatted");
                Environment.Exit(3);
            }
            foreach(string s in bytes)
            {
                ipAddr.Enqueue(Convert.ToByte(s));
            }
            return ipAddr.ToArray();
        }

        public static string IPAddrToString(byte[] ipAddr)
        {
            StringBuilder strBuilder = new StringBuilder();

            foreach (byte b in ipAddr)
            {
                strBuilder.Append(b);
                strBuilder.Append('.');
            }

            return strBuilder.ToString();
        }
    }

    // acts as a wrapper for info when sending it over async callbacks
    public class ClientObject
    {
        // client socket
        public Socket workSocket = null;
        // size of the buffer
        public const int BufferSize = 256;
        // Recieve buffer
        public byte[] buffer = new byte[BufferSize];
        // received data string
        public StringBuilder sb = new StringBuilder();
    }    
}
