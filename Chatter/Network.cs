﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace Chatter
{
    public class Network
    {
        /// <summary>
        /// Use this method to quickly get the IPV4 of the address
        /// of the machine this program is being run from
        /// </summary>
        /// <returns> Returns local IP address </returns>
        public static IPAddress GetLocalIPAddr()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach(var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    return ip;
            }
            throw new Exception("Unable to find local ip address");
        }

        /// <summary>
        /// Converts an ip in the form of a string into a the IPAdress class
        /// </summary>
        /// <param name="ip"></param>
        /// <returns> A new instance of an IPAddress with the ip given </returns>
        public static IPAddress GetRemoteIP(string ip)
        {
            IPAddress ipAddr = null;
            ipAddr = new IPAddress(StringToByteArray(ip));
            return ipAddr;
        }

        /// <summary>
        /// Converts a string representation of a string into an array
        /// of bytes to be used by the constructor for IPAddress
        /// Only converts IPV4 addresses
        /// </summary>
        /// <param name="ip"></param>
        /// <returns> An array of bytes representing an IP Address</returns>
        private static byte[] StringToByteArray(string ip)
        {
            string[] arry = ip.Split('.');
            if (arry.Length != 4)
                throw new Exception("Given IP address is malformed. byte.byte.byte.byte");
            byte[] bytes = new byte[arry.Length];
            for(int i=0;i<arry.Length;i++)
            {
                bytes[i] = Convert.ToByte(arry[i]);
            }
            return bytes;
        }

        /// <summary>
        /// A replcement to IPAddress.ToString() cause that doesn't work
        /// There is also IPAddress.Parse, damnit
        /// </summary>
        /// <param name="ip"></param>
        /// <returns> A nice IP Address reprsentation </returns>
        public static string IPAddressToString(IPAddress ip)
        {
            if (ip.AddressFamily != AddressFamily.InterNetwork)
                throw new Exception("Cannot parse non IPv4 ip address");

            StringBuilder sb = new StringBuilder();
            byte[] ipInBytes = ip.GetAddressBytes();
            for (int i = 0; i < ipInBytes.Length; i++)
            {
                sb.Append(ipInBytes[i]);
                if (i < ipInBytes.Length - 1)
                    sb.Append('.');
            }

            return sb.ToString();
        }

        public static IPAddress GetAddressTypeFromArray(IPAddress[] ips, AddressFamily fam)
        {
            foreach(IPAddress i in ips)
            {
                if (i.AddressFamily == fam)
                    return i;
            }
            throw new Exception(String.Format("No IPs found with address family {0}", fam));
        }

        /// <summary>
        /// Wraps the buffer and client into one class for easy
        /// packaging when using Async Callbacks
        /// </summary>
        public class StateObject
        {
            // client socket
            public Socket client;
            // size of the buffer
            public const int BufferSize = 256;
            // recieve buffer
            public byte[] buffer = new byte[BufferSize];
            // received data string
            public StringBuilder sb = new StringBuilder();
        }
    }
}
