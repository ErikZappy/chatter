﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Chatter
{
    class ChatterClient
    {
        // issue is being caused by the async client class
        // server is working just fine

        public class AysncClient
        {
            private const int port = 11011;
            private static ManualResetEvent connectDone =
                new ManualResetEvent(false);
            private static ManualResetEvent sendDone =
                new ManualResetEvent(false);

            public static void StartClient()
            {
                // trying to connect to a remote device
                try
                {
                    // establish the remote endpoint for the socket
                    IPHostEntry ipHostInfo = Dns.GetHostEntry("thesuperfox.zapto.org");
                    IPAddress ipAddr = Network.GetAddressTypeFromArray(ipHostInfo.AddressList, AddressFamily.InterNetwork);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddr, port);

                    // Create the TCP/IPv4 socket
                    Socket client = new Socket(AddressFamily.InterNetwork,
                        SocketType.Stream, ProtocolType.Tcp);
                    Console.WriteLine("Attempting to connect");

                    // connecting to the remote endpoint
                    client.BeginConnect(remoteEP,
                        new AsyncCallback(ConnectCallback), client);
                    connectDone.WaitOne();

                    // loops over to keep sending the server stuff until i "Quit"
                    Chat(client);

                    // shutting the socket 
                    // I have to make sure i shut down the socket***
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            public static void ConnectCallback(IAsyncResult ar)
            {
                try
                {
                    // getting the socket from the state object
                    Socket client = (Socket)ar.AsyncState;

                    // completing the connection
                    // this might be the reason i'm getting the socket
                    // error, i'll keep it for now and then see what
                    // whats when i try to send data to the server
                    client.EndConnect(ar);

                    Console.WriteLine("Socket connected to {0}",
                        client.RemoteEndPoint.ToString());

                    // signal that the cnnection is complete
                    connectDone.Set();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Send(Socket client, string msg)
            {
                // converting the string to byte data
                byte[] data = Encoding.ASCII.GetBytes(msg);

                // Begin sending the data to the remote device
                client.BeginSend(data, 0, data.Length, 0,
                    new AsyncCallback(SendCallback), client);
            }

            private static void SendCallback(IAsyncResult ar)
            {
                // retrieving the state from ar
                Socket client = (Socket)ar.AsyncState;

                // complete the sending of data to the server
                int bytesSent = client.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to the server", bytesSent);

                // signlal that all bytes have been sent
                sendDone.Set();
            }

            private static void Chat(Socket client)
            {
                String msg = String.Empty;
                while(!msg.Equals("Quit"))
                {
                    msg = Console.ReadLine();
                    Send(client, msg);
                }
                Send(client, "<EOF>");
            }
        }

        public class TestClient
        {
            // The port number for the remote device.
            private const int port = 11011;

            // ManualResetEvent instances signal completion.
            private static ManualResetEvent connectDone =
                new ManualResetEvent(false);
            private static ManualResetEvent sendDone =
                new ManualResetEvent(false);
            private static ManualResetEvent receiveDone =
                new ManualResetEvent(false);

            // The response from the remote device.
            private static String response = String.Empty;

            public static void StartClient()
            {
                // Connect to a remote device.
                try
                {
                    // Establish the remote endpoint for the socket.
                    // The name of the 
                    // remote device is "host.contoso.com".
                    IPHostEntry ipHostInfo = Dns.GetHostEntry("thesuperfox.zapto.org");
                    IPAddress ipAddress = Network.GetAddressTypeFromArray(ipHostInfo.AddressList, AddressFamily.InterNetwork);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                    // Create a TCP/IP socket.
                    Socket client = new Socket(AddressFamily.InterNetwork,
                        SocketType.Stream, ProtocolType.Tcp);

                    // Connect to the remote endpoint.
                    client.BeginConnect(remoteEP,
                        new AsyncCallback(ConnectCallback), client);
                    connectDone.WaitOne();

                    // Send test data to the remote device.
                    Send(client, "This is a test<EOF>");
                    sendDone.WaitOne();

                    // Receive the response from the remote device.
                    //Receive(client);
                    //receiveDone.WaitOne();

                    // Write the response to the console.
                    Console.WriteLine("Response received : {0}", response);

                    // Release the socket.
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void ConnectCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete the connection.
                    client.EndConnect(ar);

                    Console.WriteLine("Socket connected to {0}",
                        client.RemoteEndPoint.ToString());

                    // Signal that the connection has been made.
                    connectDone.Set();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Receive(Socket client)
            {
                try
                {
                    // Create the state object.
                    Network.StateObject state = new Network.StateObject();
                    state.client = client;

                    // Begin receiving the data from the remote device.
                    client.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void ReceiveCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the state object and the client socket 
                    // from the asynchronous state object.
                    Network.StateObject state = (Network.StateObject)ar.AsyncState;
                    Socket client = state.client;

                    // Read data from the remote device.
                    int bytesRead = client.EndReceive(ar);

                    if (bytesRead > 0)
                    {
                        // There might be more data, so store the data received so far.
                        state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                        // Get the rest of the data.
                        client.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                    }
                    else
                    {
                        // All the data has arrived; put it in response.
                        if (state.sb.Length > 1)
                        {
                            response = state.sb.ToString();
                        }
                        // Signal that all bytes have been received.
                        receiveDone.Set();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            private static void Send(Socket client, String data)
            {
                // Convert the string data to byte data using ASCII encoding.
                byte[] byteData = Encoding.ASCII.GetBytes(data);

                // Begin sending the data to the remote device.
                client.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), client);
            }

            private static void SendCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket client = (Socket)ar.AsyncState;

                    // Complete sending the data to the remote device.
                    int bytesSent = client.EndSend(ar);
                    Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                    // Signal that all bytes have been sent.
                    sendDone.Set();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
