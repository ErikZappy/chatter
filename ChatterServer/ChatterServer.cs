﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Chatter
{
    /// <summary>
    /// Main class with all the necessary components to run the server
    /// </summary>
    public class ChatterServer
    {
        protected const int port = 11011;
        public class AsyncServer
        {
            // Thread signalling
            public static ManualResetEvent connectDone = new ManualResetEvent(false);

            public static void StartServer()
            {
                // data buffer for incoming data
                byte[] bytes = new byte[Network.StateObject.BufferSize];
                IPAddress ipAddr = Network.GetLocalIPAddr();
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, port);

                // creating TCP/IPV4 socket
                Socket listener = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // bind socket to the endpoint
                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(100);

                    while (true)
                    {
                        Console.WriteLine("Waiting for connection {0}", localEndPoint.ToString());

                        // resetting the connection
                        connectDone.Reset();

                        // start async socket to listen for connections
                        listener.BeginAccept(
                            new AsyncCallback(ConnectCallback),
                            listener);

                        // starting to connect
                        connectDone.WaitOne();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                // can't forget to close this
                listener.Close();
            }

            private static void ConnectCallback(IAsyncResult ar)
            {
                // signalling the main thread to keep going
                // it'll keep trying to make connection while this
                // does it's own thing
                connectDone.Set();

                // Trying to create a new socket that will handle(ayyy)
                // the clients request
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // creating a state object to handle the buffer
                // and data from a client
                Network.StateObject state = new Network.StateObject();
                state.client = handler;

                // receiving the payload or doing something else
                handler.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize,
                    0, new AsyncCallback(RecieveCallback), state);
            }

            private static void RecieveCallback(IAsyncResult ar)
            {
                string msg = "";
                // getting the state
                Network.StateObject state = (Network.StateObject)ar.AsyncState;
                Socket handler = state.client;

                int bytesRead = handler.EndReceive(ar);

                if(bytesRead > 0)
                {
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // check for end of file tag.
                    msg = state.sb.ToString();
                    if (msg.IndexOf("<EOF>") > -1)
                    {
                        // connection should be terminated
                        string ip = Network.IPAddressToString(((IPEndPoint)handler.RemoteEndPoint).Address);
                        Console.WriteLine("Terminating connection with {0}", ip);
                    }
                    else
                    {
                        Console.WriteLine("{0}", msg);
                        state.sb.Clear();
                        // trying to get more data for msg
                        handler.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize,
                            0, new AsyncCallback(RecieveCallback), state);
                    }
                }
            }
        }

        public class TestServer
        {
            public static ManualResetEvent allDone = new ManualResetEvent(false);
            public static void StartListening()
            {
                // Data buffer for incoming data.
                byte[] bytes = new Byte[1024];

                // Establish the local endpoint for the socket.
                // The DNS name of the computer
                // running the listener is "host.contoso.com".
                IPAddress ipAddress = Network.GetLocalIPAddr();
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11011);

                // Create a TCP/IP socket.
                Socket listener = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                // Bind the socket to the local endpoint and listen for incoming connections.
                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(100);

                    while (true)
                    {
                        // Set the event to nonsignaled state.
                        allDone.Reset();

                        // Start an asynchronous socket to listen for connections.
                        Console.WriteLine("Waiting for a connection...");
                        listener.BeginAccept(
                            new AsyncCallback(AcceptCallback),
                            listener);

                        // Wait until a connection is made before continuing.
                        allDone.WaitOne();
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                Console.WriteLine("\nPress ENTER to continue...");
                Console.Read();

            }

            public static void AcceptCallback(IAsyncResult ar)
            {
                // Signal the main thread to continue.
                allDone.Set();

                // Get the socket that handles the client request.
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // Create the state object.
                Network.StateObject state = new Network.StateObject();
                state.client = handler;
                handler.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
            }

            public static void ReadCallback(IAsyncResult ar)
            {
                String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                Network.StateObject state = (Network.StateObject)ar.AsyncState;
                Socket handler = state.client;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read 
                    // more data.
                    content = state.sb.ToString();
                    if (content.IndexOf("<EOF>") > -1)
                    {
                        // All the data has been read from the 
                        // client. Display it on the console.
                        Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                            content.Length, content);
                    }
                    else
                    {
                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, Network.StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), state);
                    }
                }
            }

            private static void Send(Socket handler, String data)
            {
                // Convert the string data to byte data using ASCII encoding.
                byte[] byteData = Encoding.ASCII.GetBytes(data);

                // Begin sending the data to the remote device.
                handler.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), handler);
            }

            private static void SendCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.
                    Socket handler = (Socket)ar.AsyncState;

                    // Complete sending the data to the remote device.
                    int bytesSent = handler.EndSend(ar);
                    Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}